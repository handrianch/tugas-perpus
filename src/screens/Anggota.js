import React, { Component } from 'react';
import { View, TextInput, StyleSheet, Button, ScrollView, Modal } from 'react-native';
import RadioForm from 'react-native-simple-radio-button';
import Element from '../components/Element';
import axios from 'axios';
import { host } from '../config';

export default class Anggota extends Component {
    constructor(props) {
        super(props);
        this.state = {
            anggota: {
                username: '',
                nama_anggota: '',
                gender: '',
                no_telp: '',
                alamat: '',
                email: '',
                password: ''
            },
            listAnggota: [],
            modalVisible: false,
            gender: [
                {label: 'Laki - Laki', value: 'Laki-Laki' },
                {label: 'Perempuan', value: 'Perempuan' }
            ]
        }
    }

    async getAnggotaData() {
        const response = await axios.get(`${host}/anggota`);
        this.setState({ listAnggota: response.data });
    }

    async componentDidMount() {
        try {
            await this.getAnggotaData();
        } catch (err) {
            alert(JSON.stringify(err.message));
        }
    }

    async postAnggota() {
        const payload = this.state.anggota;
        try {
            await axios.post(`${host}/anggota`, payload);
        } catch (err) {
            alert(err.message);
        }
        this.setState({
            modalVisible: false,
            anggota: {
                username: '',
                nama_anggota: '',
                gender: '',
                no_telp: '',
                alamat: '',
                email: '',
                password: ''
            }
        });
        this.getAnggotaData();
    }

    render() {
        return (
                <View style={styles.container}>
                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.state.modalVisible}
                        onRequestClose={() => this.setState({ modalVisible: false })}
                    >
                        <View style={styles.wrapperInsideModal}>
                            <View>
                                <TextInput
                                    style={styles.inputText}
                                    placeholder="Username"
                                    onChangeText={value => this.setState({ anggota: { ...this.state.anggota, username: value } })}
                                />
                                <TextInput
                                    style={styles.inputText}
                                    placeholder="Email"
                                    onChangeText={value => this.setState({ anggota: { ...this.state.anggota, email: value } })}
                                    
                                />
                                <TextInput
                                    style={styles.inputText}
                                    placeholder="Nama Anggota"
                                    onChangeText={value => this.setState({ anggota: { ...this.state.anggota, nama_anggota: value } })}
                                />
                                <TextInput
                                    style={styles.inputText}
                                    placeholder="No Telp"
                                    onChangeText={value => this.setState({ anggota: { ...this.state.anggota, no_telp: value } })}
                                />
                                <TextInput
                                    style={styles.inputText}
                                    placeholder="Password"
                                    secureTextEntry={true}
                                    onChangeText={value => this.setState({ anggota: { ...this.state.anggota, password: value } })}
                                />
                                <RadioForm
                                    style={{ marginBottom: 8 }}
                                    radio_props={this.state.gender}
                                    initial={"Laki-Laki"}
                                    onPress={value => this.setState({ anggota: { ...this.state.anggota, gender: value } })}
                                />
                                <TextInput
                                    style={styles.inputText}
                                    placeholder="Alamat"
                                    editable
                                    multiline={true}
                                    onChangeText={value => this.setState({ anggota: { ...this.state.anggota, alamat: value } })}
                                />
                                <Button onPress={this.postAnggota.bind(this)} title="Tambah" />
                                <Button onPress={() => this.setState({ modalVisible: false })} title="Cancel" />
                            </View>
                        </View>
                    </Modal>
                    <View style={styles.wrapperInputText}>
                        <Button title="Tambah Anggota" onPress={() => this.setState({ modalVisible: true })} />
                    </View>
                    <ScrollView>
                        <View style={styles.listAnggota}>
                            {
                                this.state.listAnggota.map((item, index) => (<Element key={index} categoryName={item.username} id={item.id} />))
                            }
                        </View>
                    </ScrollView>
                </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        color: '#61DAFB',
        paddingTop: 24,
        backgroundColor: '#DEDEDE'
    },
    wrapperInputText: {
        padding: 15
    },
    listAnggota: {
        padding: 15
    },
    wrapperInsideModal: {
        marginTop: 22,
        padding: 20,
        paddingTop: 30
    },
    inputText: {
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#ababab',
        marginBottom: 8,
        paddingLeft: 10,
        paddingRight: 8,
        borderRadius: 8,
        fontSize: 15
    }
});
