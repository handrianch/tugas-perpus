import React, { Component } from 'react';
import { View, TextInput, StyleSheet, Button, ScrollView } from 'react-native';
import Element from '../components/Element';
import axios from 'axios';
import { host } from '../config';

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            kategori: '',
            listKategori: []
        }
    }

    async getKategoriData() {
        const response = await axios.get(`${host}/kategori`);
        this.setState({ listKategori: response.data });
    }

    async componentDidMount() {
        try {
            await this.getKategoriData();
        } catch (err) {
            alert(JSON.stringify(err.message));
        }
    }

    async postCategory() {
        if (this.state.kategori) {
            const payload = { category_name: this.state.kategori };
            const response = await axios.post(`${host}/api/kategori`, payload);
            this.setState({ kategori: ''})
            this.getKategoriData();
        }
    }

    setKategoriText(text) {
        this.setState({kategori: text});
    }

    render() {
        return (
                <View style={styles.container}>
                    <View style={styles.wrapperInputText}>
                        <TextInput
                            style={styles.textInputAddCategory}
                            placeholder="Tambah kategori"
                            onChangeText={this.setKategoriText.bind(this)}
                            value={this.state.text}
                        />
                        <Button onPress={this.postCategory.bind(this)} title="Tambah" />
                    </View>
                    <ScrollView>
                        <View style={styles.listCategory}>
                            {
                                this.state.listKategori.map((item, index) => (<Element key={index} categoryName={item.nama_kategori} id={item.id} />))
                            }
                        </View>
                    </ScrollView>
                </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        color: '#61DAFB',
        paddingTop: 24,
        backgroundColor: '#DEDEDE'
    },
    wrapperInputText: {
        padding: 15
    },
    textInputAddCategory: {
        borderWidth: 2,
        borderRadius: 50,
        borderBottomColor: '#3a3a3a',
        marginBottom: 10,
        paddingLeft: 30,
        paddingRight: 35,
        fontSize: 19,
        marginBottom: 20
    },
    listCategory: {
        height: 800,
        padding: 15
    }
});
