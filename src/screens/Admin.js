import React, { Component } from 'react';
import { View, TextInput, StyleSheet, Button, ScrollView, Modal } from 'react-native';
import Element from '../components/Element';
import axios from 'axios';
import { host } from '../config';

export default class Admin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            admin: {
                username: '',
                nama_admin: '',
                password: ''
            },
            listAdmin: [],
            modalVisible: false
        }
    }

    async getAdminData() {
        const response = await axios.get(`${host}/admin`);
        this.setState({ listAdmin: response.data });
    }

    async componentDidMount() {
        try {
            await this.getAdminData();
        } catch (err) {
            alert(JSON.stringify(err.message));
        }
    }

    async postAdmin() {
        if (this.state.admin.username) {
            const payload = this.state.admin;
            try {
                await axios.post(`${host}/admin`, payload);
            } catch (err) {
                alert(err.message);
            }
            this.setState({
                modalVisible: false,
                anggota: {
                    username: '',
                    nama_admin: '',
                    password: ''
                }
            });
            this.getAdminData();
        }
    }

    render() {
        return (
                <View style={styles.container}>
                    <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => this.setState({ modalVisible: false })}>
                        <View style={styles.wrapperInsideModal}>
                            <View>
                                <TextInput
                                    style={styles.inputText}
                                    placeholder="Username"
                                    onChangeText={value => this.setState({ admin: { ...this.state.admin, username: value } })}
                                />
                                <TextInput
                                    style={styles.inputText}
                                    placeholder="Nama Admin"
                                    onChangeText={value => this.setState({ admin: { ...this.state.admin, nama_admin: value } })}
                                />
                                <TextInput
                                    style={styles.inputText}
                                    placeholder="Password"
                                    secureTextEntry={true}
                                    onChangeText={value => this.setState({ admin: { ...this.state.admin, password: value } })}
                                />
                                <Button onPress={this.postAdmin.bind(this)} title="Tambah" />
                            </View>
                        </View>
                    </Modal>
                    <View style={styles.wrapperInputText}>
                        <Button title="Tambah Admin" onPress={() => this.setState({ modalVisible: true })} />
                    </View>
                    <ScrollView>
                        <View style={styles.listAdmin}>
                            {
                                this.state.listAdmin.map((item, index) => (<Element key={index} categoryName={item.username} id={item.id} />))
                            }
                        </View>
                    </ScrollView>
                </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        color: '#61DAFB',
        paddingTop: 24,
        backgroundColor: '#DEDEDE'
    },
    wrapperInputText: {
        padding: 15
    },
    listAdmin: {
        padding: 15
    },
    wrapperInsideModal: {
        marginTop: 22,
        padding: 20,
        paddingTop: 30
    },
    inputText: {
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#ababab',
        marginBottom: 8,
        paddingLeft: 10,
        paddingRight: 8,
        borderRadius: 8,
        fontSize: 15
    }
});
