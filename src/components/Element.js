import React, { Component } from 'react';
import { Text, Button, View, StyleSheet } from 'react-native';

export default class Element extends Component {
    constructor(props) {
        super(props)
        state = {
            edit: false,
            kategori: ''
        }
    }

    changeToEdit() {
        this.setState({ edit: true });
    }

    async putCategory() {
        if (this.state.kategori) {
            const payload = { category_name: this.state.kategori, id: this.props.id };
            const response = await axios.post('http://192.168.100.153/tugas-p11/update-kategori.php', payload);
            this.getKategoriData();
        }
        this.setState({ edit: false })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.left}>
                    <Text style={styles.textStyling}>{this.props.categoryName}</Text>
                </View>
                <View style={styles.right}>
                    <View style={styles.editBtn}>
                        <Button title="Edit" onPress={this.changeToEdit.bind(this)} />
                    </View>
                    <View>
                        <Button title="Delete" />
                    </View>
                </View>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginBottom: 5,
        backgroundColor: '#f2f2f2',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        padding: 10
    },
    left: {
        flex: 3,
        flexDirection: 'row',
        paddingLeft: 25
    },
    right: {
        flex: 1,
        flexDirection: 'row',
        paddingRight: 25
    },
    editBtn: {
        marginRight: 8
    },
    textStyling: {
        fontSize: 18
    }
})