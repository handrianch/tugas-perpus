/**
 * @format
 */

import {AppRegistry} from 'react-native';
// import App from './App';
// import Home from './src/screens/Home';
// import Anggota from './src/screens/Anggota';
import Admin from './src/screens/Admin';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Admin);
